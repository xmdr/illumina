struct camFollow {
	const void* subject;
	char  subjectType;
	vec3* subjectVelocity;
	vec3* subjectPosition;
	material* subjectOldMat;
	material** subjectMatPtr;
	material* trackingMat;
};

struct camPhysics {
	vec3 velocity;
	float mass;
	float springC;
	float springDampening;
	float springLength;
};

typedef struct camera{
	int locked;
	
	vec3 origin;
	vec3 lower_left_corner;
	vec3 horizontal;
	vec3 vertical;
	vec3 lookat;
	float aspect;
	float fov;

	struct camFollow follow;
	struct camPhysics physics;
} camera;

static inline void initCamera(camera * cam, vec3 lookfrom, vec3 lookat, float aspect, float fov){
	// float aspect = width/height;
	float theta = fov*M_PI/180;
	float half_height = tan(theta/2);
	float half_width = aspect * half_height;

	cam->origin = lookfrom;

	vec3 w = normalize(sub3(lookfrom, lookat)); // get negative direction that we are looking in
	if (w.x == 0 && w.y == -1 && w.z == 0){
		// the perpendicular will be a nulvector. This will mess everything up and result in a blank screen
		// if a render is attempted from such a corrupted camera. So We can't cross it with {0,1,0} but
		// there is no way to know what to cross the vector by in this case. So, change nothing and throw a soft error!
		printf("[error] Can not init camera correctly when look direction is up!\n");
		abort();
	} else {
		vec3 u = normalize(cross3((vec3){0,1,0}, w)); // get vector perpendicular to that and UP
		vec3 v = normalize(cross3(w, u)); // get direction that is "down" relative to the camera

		cam->lower_left_corner = sub3(sub3(sub3(lookfrom, scale3(u, half_width)), scale3(v, half_height)), w);
		//   lower_left_corner = origin - u*half_width - v*half_height - w
		cam->horizontal = scale3(u, 2*half_width);
		cam->vertical = scale3(v, 2*half_height);
	}

	// always copy these properties over.
	cam->aspect = aspect;
	cam->fov = fov;
	cam->lookat = lookat;
}

static inline void positionCamera(camera *cam, vec3 position, vec3 lookat){
	initCamera(cam, position, lookat, cam->aspect, cam->fov);
}

static inline void updateCamera(camera * cam){
	initCamera(cam, cam->origin, cam->lookat, cam->aspect, cam->fov);
}

static inline void translateCamera(camera * cam, vec3 difference){
	initCamera(cam, add3(cam->origin, difference), add3(cam->lookat, difference), cam->aspect, cam->fov);
}

static inline void rotateCamera(camera * cam, float d_inclination, float d_azimuth){
	// get current lookat as spherical coordinates 
	// add coords
	// transform back to cartesian

	// (0,y,0) <- inclination handles this
	// (x,0,z) <- azimuth handles this
	vec3 rel = sub3(cam->lookat, cam->origin);

	float r = magnitude(rel);
	float azimuth = atan2(rel.z, rel.x) + d_azimuth;
	float inclination = atan2( mysqrt(rel.x*rel.x + rel.z*rel.z), rel.y ) + d_inclination;

	inclination = clampf(inclination, 0.001, M_PI-0.001);

	cam->lookat = add3(cam->origin, (vec3){
		r*sin(inclination)*cos(azimuth),
		r*cos(inclination),
		r*sin(inclination)*sin(azimuth),
	});

	initCamera(cam,
		cam->origin,
		cam->lookat,
		cam->aspect,
		cam->fov
	);
}

static inline vec3 cameraToWorld(camera * cam, float u, float v){
	return add3(add3(cam->lower_left_corner, scale3(cam->horizontal, u)), scale3(cam->vertical, v));
	// lower_left_corner + u*horizontal + v*vertical
}

void cameraStopFollowing(camera* cam){
	if (cam->follow.subject != NULL) {
		printf("Stopped tracking %p.\n", cam->follow.subject);
		//*(cam->follow.subjectMatPtr) = cam->follow.subjectOldMat;
		cam->follow.subject       = NULL;
		cam->follow.subjectType   = '\0';
		cam->follow.subjectVelocity = NULL;
		cam->follow.subjectPosition = NULL;
		cam->follow.subjectMatPtr = NULL;
		cam->follow.subjectOldMat = NULL;
	}
}

void stepCamera(camera* const cam, const float dt){
	// handle camera following
	// camera is like a ball attached to 'target' via a spring
	// natural spring length is 'cam->follow.subject_distance'

	if (cam->follow.subject != NULL) {

		vec3 targetPos;
		vec3 targetLook;

		switch (cam->follow.subjectType){
			case 's': 
				targetPos = sub3(
					*(cam->follow.subjectPosition),
					scale3(normalize(*(cam->follow.subjectVelocity)),10)
				);
				targetLook = *(cam->follow.subjectPosition);
				break;
			default:
				printf("[Physics] Corruption in cam->follow\n");
				abort();
		}


		const vec3 delta = sub3(targetPos, cam->origin);
		const float distance = magnitude(delta);

		const float stretch = distance - cam->physics.springLength;
		const vec3 Fd = scale3(cam->physics.velocity, -cam->physics.springDampening);
		const vec3 Fs = scale3(normalize(delta), stretch*cam->physics.springC);
		const vec3 accel = scale3(add3(Fd, Fs), 1.f/cam->physics.mass);

		const vec3 newOrigin = add3(cam->origin, scale3(cam->physics.velocity, dt));
		const vec3 newVelocity = add3(cam->physics.velocity, scale3(accel, dt));
		
		while(cam->locked){
			printf("[Physics] Waiting for camera to unlock!\n");
			// do nothing and wait until unlocked
		}
		cam->lookat = targetLook;
		cam->physics.velocity = newVelocity;
		cam->origin = newOrigin;
		updateCamera(cam);
		cam->locked = 0;
	}
}
