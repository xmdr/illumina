#pragma once

#include "vector.h"

static inline
char raycast(
	const vec3 origin, const vec3 direction, // in
	float *const distance, vec3 *const normal, const primitive **const obj  // out
){
	const primitive *hit_p = NULL;
	float            hit_d = MAX_RENDER_DISTANCE;
	char             hit_t = 't';

	if (workspace->bvh != 0){
		vec3 inv_dir = inv3(direction);
		if (iAABB(origin, inv_dir, workspace->bb)){
			iBVH(origin, direction, inv_dir, workspace->bvh, &hit_d, &hit_p);
		}
	}

	const sphere *si = workspace->spheres;
	const sphere *const sn = workspace->spheres+workspace->sphereCount;
	while (si < sn){
		const float tmp_d = iSphere(origin, direction, si);
		if (tmp_d < hit_d && tmp_d > NEAR_ZERO){
			hit_d = tmp_d;
			hit_p = (const primitive*)si;
			hit_t = 's';
		}
		si++;
	}

	if (hit_d == MAX_RENDER_DISTANCE){
		// there was nothing nearer than MAX_RENDER_DISTANCE
		return '\0';
	}

	if (hit_t == 's'){
		*normal = nSphere((sphere*)hit_p, add3(scale3(direction, hit_d), origin));
	} else {
		*normal = ((triangle*)hit_p)->normal;
	}

	*distance = hit_d;
	*obj = hit_p;
	return hit_t;
}

static inline vec3 lambert(vec3 normal){
	vec3 v;
	do {
		v = normalize(add3(normal, random3OnUnitSphere()));
#ifndef BACKFACE_CULLING
		// Potential fixme: test often fails, even when attemting add3(scale3(normal, 1+R_OFFS), r3u);
		// when backfaces are not culled, this results in shadow acne
		// (triangle falsely self-shadows since reflection ray is spawned underneath triangle, trapped in darkness)
	} while(dot3(v,normal) < NEAR_ZERO);
#else
	} while(0);
#endif
	return v;
}

static inline void calcReflectionDirAndWeight(
	const float roughness, const float cos_theta_i, const vec3 normal, const vec3 direction, // inputs
	vec3 *const restrict newdir, vec3 *const samplecol // outputs
){
	// calculates the reflected ray direction and attenuates the material color by the probability of said direction

	if (roughness > 0){
		// importance sampling: shoot a ray into a light source direction and weight it according to it's normal rarity
		#ifdef SUN_BIAS
		if (random01() <= SUN_BIAS){
			const float pdf_sun    = M_PI * (1 - SUN_COSTHETA);
			const float weight_sun = fmaxf(0, dot3(normal, SUN_DIR))*pdf_sun;
			// This approximation breaks down when roughness != 1, because it assumes a cosine weighted hemisphere as the default PDF (PDF=1)
			// Potential todo: divide the weight by the material PDF to support non-matte materials
			// to importance sample other objects, replace SUN_COSTHETA with the appropriate solid angle formula
			// Potential todo: just return a float weight, so this function does not need samplecol
			*samplecol = scale3(*samplecol, weight_sun);
			*newdir = SUN_DIR;
		} else
		#endif
		{
			(void)samplecol;
			// this is equivalent to cosine weighted hemisphere
			// cosine weighted hemisphere distr == uniformly weighted 0.5*normal + 0.5*unitsphere distr
			// since it's uniform, the PDF is 1
			const vec3 diffuse = lambert(normal);
			const vec3 reflect = sub3(direction, scale3(normal, 2*cos_theta_i));
			*newdir = normalize(lerp3(reflect, diffuse, roughness));
		}
	} else {
		(void)samplecol;
		const vec3 reflect = sub3(direction, scale3(normal, 2*cos_theta_i));
		*newdir = reflect;
	}
}

static inline vec3 raytrace(vec3 origin, vec3 direction, unsigned recursionD)
{
	// for each object in the scene, see if the ray intersects with its geometry
	// if it does, calculate how long the ray is
	// if the ray is shorter than all previous rays, overwrite object pointer and length variable
	// after the the iteration, shade the pixel with the material of that nearest geometry

	if (recursionD > MAX_DEPTH){
		return (vec3){0,0,0};
	}

	float distance; vec3 normal; const primitive *obj;
	if (raycast(origin, direction, &distance, &normal, &obj) == '\0'){
		return workspace->bkgTexture(direction);
	}

	const vec3 hitpoint = add3(scale3(direction, distance), origin);
	material mat = *obj->mat;
	mat.color = mat.texture ? mat.texture(hitpoint) : mat.color;

#ifdef FLAT_CASTING
	return mat.color;
#endif
	if (mat.color.x > 1 || mat.color.y > 1 || mat.color.z > 1){
		// emmisive material: don't bother with the relatively tiny contribution of reflection/refraction
		return mat.color;
	}

	float cos_theta_i = dot3(direction, normal);
	float eta = 1.f/mat.IOR;
	if (cos_theta_i > 0){
		// we are exiting the object, hence direction and normal align
		// negate since direction and normal should be "opposing"
		cos_theta_i = -cos_theta_i;
		normal = neg3(normal);
		eta = 1.f/eta;
	}

	const float discriminant = 1.f - eta*eta*(1.f-cos_theta_i*cos_theta_i);
	if (discriminant < NEAR_ZERO){
		mat.reflectance = 1; // total internal reflection
	}

	// Schlick approximation (but cos theta is negative) attenuates reflection near grazing angles
	const float fresnel = pow(1.0+cos_theta_i, 5.0);        
	mat.reflectance = lerpf(mat.reflectance, 1.0, fresnel);
	mat.color       = lerp3(mat.color, (vec3){1,1,1}, fresnel);
	mat.roughness  *= (1-fresnel);
	
	vec3 color = (vec3){0,0,0};
	const size_t n = (recursionD == 0)?SAMPLES:1; // run SAMPLES samples when d=0, else 1 sample per bounce
	// this way, we simulate SAMPLES paths per pixel, without recalculating the first hit SAMPLES times
	for (size_t i=0; i<n; i++){
		vec3 newdir;
		vec3 samplecol = mat.color; // × 1
		float offset = R_OFFS;

		// fixme: when a ray exits a transmissive object, this code now acts like there is gloss on the inside
		// probably rare since glass materials should not have any gloss, but still.

		// pick randomly: reflecting off of the gloss, or transmitting through
		if (mat.gloss_reflectance > 0 && random01() <= lerpf(mat.gloss_reflectance, 1.0, fresnel)){
			samplecol = (vec3){1,1,1}; // Potential todo: add mat.gloss_color?
			calcReflectionDirAndWeight(mat.gloss_roughness*(1-fresnel), cos_theta_i, normal, direction, &newdir, &samplecol);
		} else {
			// ray reached base material underneath gloss
			// pick randomly: reflection or refraction
			if (mat.reflectance == 1.f || random01() <= mat.reflectance){
				calcReflectionDirAndWeight(mat.roughness, cos_theta_i, normal, direction, &newdir, &samplecol);
			} else {
				// cos_theta_i already negative, negate again
				newdir = normalize(add3(scale3(direction, eta), scale3(normal, -eta*cos_theta_i - sqrtf(discriminant))));
				offset = -offset;
			}
		}
		const vec3 neworigin = add3(hitpoint, scale3(normal, offset));
		const vec3 bounce = raytrace(neworigin, newdir, recursionD+1);
		color = add3(color, mult3(samplecol, bounce));
	}

	return scale3(color, 1.f/n);
}
