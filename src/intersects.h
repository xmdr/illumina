#pragma once

#include <stdbool.h>

#include "geometry.h"
#include "bvh.h"

// tried and tested "faster" algorithms that are slower or produce artifacts/bugs:
// weber algorithm introduces specks/dots
// this algorithm: http://geomalgorithms.com/a06-_intersect-2.html (too slow)

static inline float iTriangle(const vec3 origin, const vec3 direction, const triangle *const tri)
{
#ifdef BACKFACE_CULLING
	if (dot3(direction, tri->normal) > 0){
		return MAX_RENDER_DISTANCE;
	}
#endif
	const vec3 v0v1 = sub3(tri->v1, tri->v0);
	const vec3 v0v2 = sub3(tri->v2, tri->v0);
	const vec3 pvec = cross3(direction, v0v2);
	const float det = dot3(v0v1, pvec);

	// ray and triangle are parallel if det is close to 0
	if (fabsf(det) < NEAR_ZERO){
		return MAX_RENDER_DISTANCE;
	}

	const float rDet = 1.f/det;
	const vec3  tvec = sub3(origin, tri->v0);
	const vec3  qvec = cross3(tvec, v0v1);
	const float u    = dot3(tvec, pvec) * rDet;
	const float v    = dot3(direction, qvec) * rDet;
	if ((v < -NEAR_ZERO) | (u+v > 1+NEAR_ZERO) | (u < -NEAR_ZERO) | (u > 1+NEAR_ZERO)){
		return MAX_RENDER_DISTANCE;
	}

	return dot3(v0v2, qvec) * rDet;
}

static inline float iSphere(const vec3 origin, const vec3 direction, const sphere *const sph)
{
	const vec3 omp = sub3(origin, sph->position);
	const float a = dot3(direction, direction);
	const float b = 2 * dot3(direction, omp);
	const float c = dot3(omp, omp) - sph->radius*sph->radius;
	float x0,x1;

	const int sol = solveQuadratic(a, b, c, &x0, &x1);
	if(!sol){
		return MAX_RENDER_DISTANCE;
	} else {
		return fminf(x0,x1);
	}
}

static inline bool iAABB(const vec3 ro, const vec3 rd_inv, AABB bb) {
	const float t1 = (bb.min.x - ro.x) * rd_inv.x;
	const float t2 = (bb.max.x - ro.x) * rd_inv.x;
	const float t3 = (bb.min.y - ro.y) * rd_inv.y;
	const float t4 = (bb.max.y - ro.y) * rd_inv.y;
	const float t5 = (bb.min.z - ro.z) * rd_inv.z;
	const float t6 = (bb.max.z - ro.z) * rd_inv.z;

	const float tmin = fmaxf(fmaxf(fminf(t1, t2), fminf(t3, t4)), fminf(t5, t6));
	const float tmax = fminf(fminf(fmaxf(t1, t2), fmaxf(t3, t4)), fmaxf(t5, t6));

	return tmax >= 0 && tmin <= tmax;
}

static inline
void iBVH(
	const vec3 origin, const vec3 direction, const vec3 inv_dir, const BVH *node,
	float *hit_d, const primitive **const hit_p
){
	const BVH bvh = *node;
	if (bvh.triangles == NULL) {
		int lhit = iAABB(origin, inv_dir, bvh.lbb);
		int rhit = iAABB(origin, inv_dir, bvh.rbb);
		if (lhit + rhit == 0) return;
		if (lhit + rhit == 1) {
			iBVH(origin, direction, inv_dir, lhit?bvh.lt:bvh.rt, hit_d, hit_p);
		} else {
			iBVH(origin, direction, inv_dir, bvh.lt, hit_d, hit_p);
			iBVH(origin, direction, inv_dir, bvh.rt, hit_d, hit_p);
		}
	} else {
		float hit_d_cache = *hit_d;
		const primitive *hit_p_cache = *hit_p;

		const triangle *const tn = bvh.triangles + bvh.n;
		for (const triangle *ti=bvh.triangles; ti<tn; ti++){
			const float tmp_d = iTriangle(origin, direction, ti);
			if (tmp_d < hit_d_cache && tmp_d > NEAR_ZERO){
				hit_d_cache = tmp_d;
				hit_p_cache = (const primitive *)ti;
			}
		}
		*hit_d = hit_d_cache;
		*hit_p = hit_p_cache;
	}
}
