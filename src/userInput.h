static int ui_thread_main(void* datapt){
	printf("UI thread starting!\n");
	struct thread_data* data = *(struct thread_data**)datapt;

	camera* cam = data->workspace->cam;
	int* demoRunning = data->demoRunning;
	SDL_Window* window = data->window;

	vec3 nullvec = (vec3){0,0,0};
	vec3 upvec   = (vec3){0,1,0};

	int uiTimer=0, panDebounce=0, panDebounceDelay=7, genericDebounceDelay=100;

	// keep track of mouse state
	int scroll = 0;
	float panningSpeed = 1;
	int mouseDownLeft  = 0;
	int mouseDownRight = 0;
	int relativeMouse  = 0;
	float cursorX=0, cursorY=0;
	int canvasW=0, canvasH=0;
	float mouseSensitivity = 0.5;

	SDL_Event event;
	const uint8_t *kbd = SDL_GetKeyboardState(NULL);

	while(*demoRunning){

		// handle keyboard
		#ifdef ENABLE_PHYSICS
		if (kbd[SDL_SCANCODE_F]) {
			static int debounce = 0;
			if (debounce < uiTimer){
				debounce = uiTimer + genericDebounceDelay;

				if (kbd[SDL_SCANCODE_LSHIFT]) {
					cameraStopFollowing(cam);
				} else {
					if (cam->follow.subject != NULL){
						printf("Did not initiate tracking: already tracking an object.\n");
					} else {
						// start tracking new object
						float dist = MAX_RENDER_DISTANCE;
						const primitive *hit = NULL;
						vec3 norm;
						vec3 dir = normalize(sub3(cameraToWorld(cam, cursorX, 1.f-cursorY), cam->origin));
						char hitType = raycast(cam->origin, dir, &dist, &norm, &hit);

						if (hitType != '\0'){
							cam->follow.subject = hit;
							cam->follow.subjectType = hitType;
							switch (cam->follow.subjectType){
								case 's':
									cam->follow.subjectVelocity = &(((sphere*)cam->follow.subject)->velocity);
									cam->follow.subjectPosition = &(((sphere*)cam->follow.subject)->position);
									cam->follow.subjectMatPtr = &(((sphere*)cam->follow.subject)->mat);
									break;
								case 't':
									cam->follow.subjectVelocity = &nullvec;
									cam->follow.subjectPosition = &nullvec;
									cam->follow.subjectMatPtr = &(((triangle*)cam->follow.subject)->mat);
									break;
							}

							if (magnitude(*cam->follow.subjectVelocity) > NEAR_ZERO) {
								//cam->follow.subjectOldMat = *cam->follow.subjectMatPtr;
								//*cam->follow.subjectMatPtr = cam->follow.trackingMat;
								printf("Currently tracking %c %p\n", cam->follow.subjectType, cam->follow.subject);
							} else {
								cam->follow.subject = NULL;
								cam->follow.subjectVelocity = &nullvec;
								cam->follow.subjectPosition = &nullvec;
								printf("Did not initiate tracking: object standing still.\n");
							}
						} else {
							printf("Did not initiate tracking: no object found under cursor.\n");
						}
					}
				}
			}
		}
		#endif


		if (kbd[SDL_SCANCODE_BACKSPACE]) {
			// remove spheres
			static int debounce = 0;
			if (debounce < uiTimer){
				debounce = uiTimer + genericDebounceDelay;

				printf("[UI thread] backspace was pressed!\n");
				cameraStopFollowing(cam);
				clearAllSpheres(data->workspace);
				printf("[UI thread] ALL spheres have been DELETED!\n");
			}
		}

		if (kbd[SDL_SCANCODE_SPACE]) {
			// make sphere in front of cam
			static int debounce = 0;
			if (debounce < uiTimer){
				debounce = uiTimer + genericDebounceDelay;

				vec3 direction = normalize(sub3(cam->lookat, cam->origin));
				makeSphere(
					data->workspace,
					add3(cam->origin, scale3(direction,25.f)),
					10.f,
					scale3(direction,25.f),
					NULL
				);
				printf("[UI thread] Made a sphere! Total: %zu\n", data->workspace->sphereCount);
			}
		}

		if (kbd[SDL_SCANCODE_RETURN]) {
			static int debounce = 0;
			if (debounce < uiTimer){
				debounce = uiTimer + genericDebounceDelay;

				printf("cam.origin   = ");
				printvec(cam->origin);
				printf("cam.lookat   = ");
				printvec(cam->lookat);
				printf("cam.velocity = ");
				printvec(cam->physics.velocity);
			}
		}

		if (kbd[SDL_SCANCODE_P]) {
			static int debounce = 0;
			if (debounce < uiTimer){
				debounce = uiTimer + genericDebounceDelay;

				while(cam->locked){
					puts("[UI thread] (mouse motion) Waiting for camera to unlock!\n");
					// do nothing and wait until unlocked
				}
				// lock the camera and the scene preventing the raytracer from overwriting it
				cam->locked = 1;
				writeScene(data->workspace);
				bitmap(data->xres, data->yres, data->texturePtr, data->pitch);
				cam->locked = 0;
				doneWritingScene(data->workspace);

			}
		}

		if(uiTimer > panDebounce){
			vec3 panVelocity = nullvec;

			// left or right: make a vector perpendicular to both up (0,1,0) and the camera's direction
			if (kbd[SDL_SCANCODE_A] || kbd[SDL_SCANCODE_LEFT]) {
				// move left relative to cam
				panVelocity = add3(panVelocity,
					cross3(upvec, sub3(cam->lookat, cam->origin))
				);
			}
			if (kbd[SDL_SCANCODE_D] || kbd[SDL_SCANCODE_RIGHT]) {
				// move right relative to cam
				panVelocity = add3(panVelocity,
					cross3(sub3(cam->lookat, cam->origin), upvec)
				);
			}

			// forwards or backwards: just take the (inverse) direction of the camera
			if (kbd[SDL_SCANCODE_W] || kbd[SDL_SCANCODE_UP]){
				// move forwards relative to cam
				panVelocity = add3(panVelocity,
					sub3(cam->lookat, cam->origin)
				);
			}
			if (kbd[SDL_SCANCODE_S] || kbd[SDL_SCANCODE_DOWN]){
				// move backwards relative to cam
				panVelocity = add3(panVelocity,
					sub3(cam->origin, cam->lookat)
				);
			}

			// up or down: make a vector perpendicular to the camera's direction and also the camera's left or right
			if (kbd[SDL_SCANCODE_E]){
				// move up relative to cam
				panVelocity = add3(panVelocity,
					cross3(cross3(sub3(cam->lookat, cam->origin), (vec3){0,1,0}), sub3(cam->lookat, cam->origin))
				);
			}
			if (kbd[SDL_SCANCODE_Q]){
				// move down relative to cam
				panVelocity = add3(panVelocity,
					cross3(cross3((vec3){0,1,0}, sub3(cam->lookat, cam->origin)), sub3(cam->lookat, cam->origin))
				);
			}

			if (magnitude(panVelocity) > NEAR_ZERO)
			{
				float modifier = 1.f;
				if (kbd[SDL_SCANCODE_LSHIFT]) {
					modifier *= 0.5f;
				} else
				if (kbd[SDL_SCANCODE_LCTRL]) {
					modifier *= 2.f;
				}

				panVelocity = scale3(normalize(panVelocity), modifier*panningSpeed);

				while(cam->locked){
					printf("[UI thread] (keyboard) Waiting for camera to unlock!\n");
					// do nothing and wait until unlocked
				}
				cam->locked = 1;
				translateCamera(cam, panVelocity);
				cam->locked = 0;
				panDebounce = uiTimer + panDebounceDelay; // just a debounce so you don't move 1000 times per second when holding a key down
			}
		}

		// handle mouse and quitevent
		while (SDL_PollEvent(&event)){
			switch (event.type){
				case SDL_QUIT:{
					printf("[UI thread] Program was sent signal to halt!\n");
					*demoRunning = 0;
					break;
				}

				case SDL_MOUSEBUTTONDOWN:{
					if(event.button.button == SDL_BUTTON_RIGHT) {
						if (!mouseDownRight){
							mouseDownRight = 1;
							SDL_CaptureMouse(SDL_TRUE);
							SDL_SetRelativeMouseMode(SDL_TRUE);
							relativeMouse = 1;
						}
					}

					if(event.button.button == SDL_BUTTON_LEFT) {
						if (!mouseDownLeft){
							mouseDownLeft = 1;

							printf("Clicked on (%f, %f): ", cursorX, cursorY);
							float dist = MAX_RENDER_DISTANCE;
							const primitive *nearest = NULL;
							vec3 norm;
							vec3 dir = normalize(sub3(cameraToWorld(cam, cursorX, 1.f-cursorY), cam->origin));
							char type = raycast(cam->origin,dir,&dist,&norm,&nearest);

							if (nearest){
								nearest->mat->color = random3();
								const char *shape = type=='t'?"triangle":type=='s'?"sphere":"quad";
								printf("a %s at %p.\n", shape, (void*)nearest);
							} else {
								printf("nothing.\n");
							}

							if(type!='\0' && kbd[SDL_SCANCODE_LSHIFT]){
								// shift-click: remove object
								switch(type){
									case 't': 
										removeTriangle(data->workspace, (triangle*)nearest);
										printf("(REMOVED)\n");
										break;
									case 's':
										removeSphere(data->workspace, (sphere*)nearest);
										printf("(REMOVED)\n");
										break;
								}
							}
						}
					}

					if(event.button.button == SDL_BUTTON_MIDDLE){

					}

					break;
				}

				case SDL_MOUSEBUTTONUP:{
					if(event.button.button == SDL_BUTTON_RIGHT) {
						mouseDownRight = 0;
						SDL_CaptureMouse(SDL_FALSE);
						SDL_SetRelativeMouseMode(SDL_FALSE);
						SDL_WarpMouseInWindow(data->window, cursorX*canvasW, cursorY*canvasH);
						relativeMouse = 0;
					}
					if(event.button.button == SDL_BUTTON_LEFT) {
						mouseDownLeft = 0;
					}
					break;
				}

				case SDL_MOUSEMOTION:{

					if (!relativeMouse){
						SDL_GetWindowSize(window, &canvasW, &canvasH);

						cursorX = (float)event.motion.x/canvasW;
						cursorY = (float)event.motion.y/canvasH;
					}
					if (mouseDownRight){
						
						const float d_inclination = mouseSensitivity*(float)event.motion.yrel*M_PI/180;
						const float d_azimuth = mouseSensitivity*(float)event.motion.xrel*M_PI/180;
						
						while(cam->locked){
							printf("[UI thread] (mouse motion) Waiting for camera to unlock!\n");
							// do nothing and wait until unlocked
						}
						cam->locked = 1;
						rotateCamera(cam, d_inclination, d_azimuth);
						cam->locked = 0;
					}
					break;
				}

				case SDL_MOUSEWHEEL:{
					if (event.wheel.y > 0 && scroll < 8){
						scroll++;
					} else
					if (event.wheel.y < 0 && scroll > -6){
						scroll--;
					}
					panningSpeed = powf(2, scroll); // leverages floating point base of 2; no precision loss
					printf("[UI thread] (mousewheel) New panningSpeed: 2^%d = %fm/s\n",scroll, panningSpeed);
					break;
				}

				default:
					break;
			}
		}

		uiTimer++;
		SDL_Delay(1);
	}

	printf("UI thread exited!\n");

	return 0;
}
