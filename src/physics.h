//#define PHYSICS_DYNAMIC_TICKRATE
// let the physics engine decide what the best simulation frequency is

//#define PHYSICS_PRINT_TICKRATE
// if enabled and dynamic tickrate is also enabled, physics thread will send periodic updates about tickrate and sim frequency

const float gravConst = 1;//.1;
//const float pAir = 1.225f;
const float pAir = 1.225f;
const float simfreq = 120;

const int attractionEnabled = 1;
const int collisionsEnabled = 1;
const int airResistanceEnabled = 0;
const int groundEnabled = 0;
const int gravityEnabled = 0;

float actualSimFreq = simfreq;

/*
float boxCalcMomentOfInertia(vec3 axis, obb * box){
	return 1;
	float h, w, d;
	float m = box->mass;
	float a = (m * (h*h + d*d))/12;
	float b = (m * (w*w + d*d))/12;
	float c = (m * (w*w + h*h))/12;

	matrix3x3 I_matrix = (matrix3x3){
		{a, 0, 0},
		{0, b, 0},
		{0, 0, c}
	};


}

float sphereCalcMomentOfInertia(vec3 axis, sphere * ball){
	matrix3x3 I_matrix = {0};
}*/

void setupPhysics(scene * wkspace)
{
	if (groundEnabled){
		// ground plane (bilinear patch))
		material * groundMaterial = &(wkspace->mats[wkspace->matCount++]);
		groundMaterial->color = (vec3){100.f/255, 220.f/255, 50.f/255};
		groundMaterial->reflectance = 0;
		groundMaterial->roughness = 0;

		float groundSize = 3000.f;
		float groundHeight = -75;

		triangle * gnd1 = &(wkspace->triangles[wkspace->triangleCount++]);
		triangle * gnd2 = &(wkspace->triangles[wkspace->triangleCount++]);

		/*
        3---1
         \  |
          \ |
           \|
            2
		*/

		gnd1->v0 = (vec3){ groundSize, groundHeight, groundSize};
		gnd1->v1 = (vec3){ groundSize, groundHeight,-groundSize};
		gnd1->v2 = (vec3){-groundSize, groundHeight, groundSize};


		gnd2->v0 = (vec3){-groundSize, groundHeight, groundSize};
		gnd2->v1 = (vec3){ groundSize, groundHeight,-groundSize};
		gnd2->v2 = (vec3){-groundSize, groundHeight,-groundSize};

		gnd1->normal = (vec3){0,1,0};
		gnd2->normal = (vec3){0,1,0};

		gnd1->mat = groundMaterial;
		gnd2->mat = groundMaterial;
	}
}

static inline void stepPhysics(scene * wkspace)
{
	sphere * b1 = NULL;
	sphere * b2 = NULL;
	float afstand = 0;
	float kracht = 0;

	for (size_t i = 0; i < wkspace->sphereCount; ++i)
	{
		b1 = &(wkspace->spheres[i]);

		for (size_t j = i+1; j < wkspace->sphereCount; ++j)
		{
			b2 = &(wkspace->spheres[j]);

			afstand = magnitude(sub3(b2->position, b1->position));

			////////////////////////
			//
			//   ATTRACTION
			
			if (attractionEnabled && afstand > 1)
			{
				// F = G * m1 * m2 * r^-2
				kracht = gravConst * b1->mass * b2->mass * powf(afstand, -2);

				b1->velocity = add3(b1->velocity, scale3(sub3(b2->position, b1->position), kracht/b1->mass/actualSimFreq) );
				b2->velocity = add3(b2->velocity, scale3(sub3(b1->position, b2->position), kracht/b2->mass/actualSimFreq) );

				// a = F * richting / massa
				// massa = 1
				// v = v + a/dt
			}

			////////////////////////
			//
			//   COLLISION

			if (collisionsEnabled) {

				// do b1 and b2 overlap?

				if ( afstand < (b1->radius + b2->radius))
				{
					// draag kin e over
					
					kracht = 10000*powf(b1->radius + b2->radius - afstand, 2);

					b1->velocity = add3(b1->velocity, scale3(sub3(b1->position, b2->position), kracht/b1->mass/actualSimFreq) );
					b2->velocity = add3(b2->velocity, scale3(sub3(b2->position, b1->position), kracht/b2->mass/actualSimFreq) );

					// a = F * richting / massa
					// massa = 1
					// v = v + a/dt
				}
			}
		}

		////////////////////////////
		//
		//   GRAVITY
		
		if (gravityEnabled)
		{
			b1->velocity.y = b1->velocity.y - 98.1/actualSimFreq;
		}

		////////////////////////////
		//
		//   AIR RESISTANCE

		if (airResistanceEnabled)
		{
			// kracht = 0.5f*pAir*speedSquared*(M_PI*b1->radius)*0.47f;
			// b1->velocity = sub3(b1->velocity, scale3(b1->velocity, kracht/b1->mass/actualSimFreq));

			b1->velocity.x -= b1->velocity.x/1000;
			b1->velocity.y -= b1->velocity.y/1000;
			b1->velocity.z -= b1->velocity.z/1000;
		}

		// Apply velocity
		b1->position = add3(b1->position, divide3(b1->velocity, actualSimFreq));

		if (groundEnabled)
		{
			// fix position
			if(b1->position.y - b1->radius < -75){
				b1->position.y = -75 + b1->radius;
				b1->velocity.y = -b1->velocity.y;
			}
		}
	}
}

static int psx_thread_main(void * datapt){
	printf("Physics thread starting!\n");

	int tickTime = (int)floor(1000.f/simfreq);
	actualSimFreq = 1000.f/(float)tickTime;
#ifdef PHYSICS_DYNAMIC_TICKRATE
	printf("[Physics] Dynamic tickrate is turned on. Sending updates every 1000 ticks. Current: %dms between steps (%fHz)\n", tickTime, actualSimFreq);
#else
	printf("[Physics] Running with max. %dms between steps (%fHz) (can only accept integer tickrates, f must be 62.5Hz, 125Hz, etc!)\n", tickTime, actualSimFreq);
#endif

	struct thread_data * data = *(struct thread_data **)datapt;

	int * demoRunning = data->demoRunning;
	unsigned long int psxStep = 0;

	int delta, wait, start = SDL_GetTicks();

	writeScene(data->workspace);
	setupPhysics(data->workspace);
	doneWritingScene(data->workspace);

	camera* cam = data->workspace->cam;

	while(*demoRunning){
		psxStep++;
		// make a copy of workspace, do physics on it,
		// then replace workspace with physics'ed scene

		writeScene(data->workspace);
		//readScene(data->workspace);
		stepPhysics(data->workspace);
		stepCamera(cam, 1.f/actualSimFreq);
		//doneReadingScene(data->workspace);
		doneWritingScene(data->workspace);

		delta = SDL_GetTicks() - start;
		start = SDL_GetTicks();
		wait = tickTime - delta;
		if (wait > 0)
		{
#ifdef PHYSICS_DYNAMIC_TICKRATE
			// we had time to spare so we can decrease time between steps for the future
			tickTime--;
			actualSimFreq = 1000.f/(float)tickTime;
#endif

			SDL_Delay(wait);
		}

#ifdef PHYSICS_DYNAMIC_TICKRATE
		else
		{
			// we did not have time to compute and lost realtime synchronization
			// so we have to increase time between steps to prevent this in the future
			tickTime++;
			actualSimFreq = 1000.f/(float)tickTime;
			//printf("[Physics] lagged %dms!\n", wait);
		}

#ifdef PHYSICS_PRINT_TICKRATE
		if(psxStep%1000 == 0){
			printf("[Physics] tick: %dms\t freq: %fHz\n", tickTime, actualSimFreq);
		}
#endif
#endif

	}

	printf("Physics thread exited!\n");
	return EXIT_SUCCESS;
}
