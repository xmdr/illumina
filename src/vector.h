#pragma once

#include <math.h>

#include "extra_math.h"
#include "random.h"

typedef struct {
	float x;
	float y;
	float z;
} vec3;

static inline float magnitude(const vec3 v){
	return mysqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

static inline float selfdot3(const vec3 v){
	return v.x*v.x + v.y*v.y + v.z*v.z;
}

static inline vec3 normalize(const vec3 v){
	const float m = magnitude(v);
	return (vec3){v.x/m, v.y/m, v.z/m};
}

static inline vec3 random3(void){
	return (vec3){random01(), random01(), random01()};
}

static inline float dot3(const vec3 v, const vec3 u){
	return v.x*u.x + v.y*u.y + v.z*u.z;
}

static inline vec3 cross3(const vec3 v, const vec3 u){
	return (vec3){
		v.y*u.z - v.z*u.y,
		v.z*u.x - v.x*u.z,
		v.x*u.y - v.y*u.x
	};
}

static inline vec3 add3(const vec3 v, const vec3 u){
	return (vec3){
		v.x + u.x,
		v.y + u.y,
		v.z + u.z
	};
}

static inline vec3 sub3(const vec3 v, const vec3 u){
	return (vec3){
		v.x - u.x,
		v.y - u.y,
		v.z - u.z
	};
}

static inline vec3 add3f(const vec3 v, const float f){
	return (vec3){
		v.x + f,
		v.y + f,
		v.z + f
	};
}

static inline vec3 sub3f(const vec3 v, const float f){
	return (vec3){
		v.x - f,
		v.y - f,
		v.z - f
	};
}

static inline vec3 scale3(const vec3 v, const float s){
	return (vec3){
		v.x*s,
		v.y*s,
		v.z*s
	};
}

static inline vec3 divide3(const vec3 v, const float s){
	return (vec3){
		v.x/s,
		v.y/s,
		v.z/s
	};
}

static inline vec3 neg3(const vec3 v){
	return (vec3){
		-v.x,
		-v.y,
		-v.z
	};
}

static inline vec3 inv3(const vec3 v){
	return (vec3){
		1.0f / v.x,
		1.0f / v.y,
		1.0f / v.z
	};
}

static inline vec3 lerp3(const vec3 v, const vec3 u, const float t){
	return (vec3){
		(1.f-t)*v.x + t*u.x, // linux confirmed
		(1.f-t)*v.y + t*u.y,
		(1.f-t)*v.z + t*u.z
	};
}

static inline vec3 mult3(const vec3 v, const vec3 u){
	// component wise multiplication
	return (vec3){
		v.x*u.x,
		v.y*u.y,
		v.z*u.z
	};
}

static inline float angle3(const vec3 v, const vec3 u){
	return acosf(dot3(v,u)/(magnitude(v)*magnitude(u)));
}

static inline void printvec(const vec3 v){
	printf("(vec3){%f, %f, %f}\n", v.x,v.y,v.z);
}

static inline vec3 abs3(const vec3 v){
	return (vec3){
		fabsf(v.x),
		fabsf(v.y),
		fabsf(v.z)
	};
}

static inline vec3 random3OnUnitSphere(void){
	// rejection sampling

	float d, x, y, z;
	do {
		x = random01() * 2.0f - 1.0f;
		y = random01() * 2.0f - 1.0f;
		z = random01() * 2.0f - 1.0f;
		d = x*x + y*y + z*z;
	} while(d > 1.0f);
	return (vec3){x/d, y/d, z/d}; // here's the catch
}

vec3 rgb2vec(const char*const h){
	long all = strtol(h,NULL,16);
	float b = all % 256;
	all /= 256;
	float g = all % 256;
	all /= 256;
	float r = all % 256;
	return (vec3){r/255,g/255,b/255};
}

static inline vec3 gammaCorrect(vec3 c, float gamma){
	return (vec3){
		clampf(pow((c.x), 1/gamma),0,1),
		clampf(pow((c.y), 1/gamma),0,1),
		clampf(pow((c.z), 1/gamma),0,1),
	};
}

static inline float luma(vec3 c){
	return mysqrt(dot3(c, (vec3){0.299f, 0.587f, 0.114f}));
}
