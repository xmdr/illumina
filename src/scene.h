#pragma once

// This is the top-level "class"
// it contains geometry soups, counts of geometry, camera, backgroundcolor, background texture function
#include <stdint.h>

#include "bvh.h"

typedef struct scene{
	uint8_t beingWritten:1;
	uint8_t readers; // how many threads are reading the scene

	camera* cam;

	triangle* triangles; // array of triangles
	sphere* spheres;
	material* mats;

	size_t triangleCount;
	size_t sphereCount;
	size_t matCount;

	AABB bb;
	BVH *bvh;

	vec3 (*bkgTexture)(vec3);// = NULL; // points to a texture function
} scene;

////////////////////////////////////////////////////////////////////////////
// multithreading: allow multiple readings at the same time from multiple
// threads, but under no circumstance will a scene be read while it is
// being written to/changing.

static inline void doNothing(void){
	asm volatile ("nop");
}

static inline void writeScene(scene * s){
	// for locking a scene
	// multithreading and all that
	while(s->beingWritten || (s->readers > 0)){doNothing();}
	s->beingWritten = 1;
}

static inline void doneWritingScene(scene * s){
	// above, but reverse
	s->beingWritten = 0;
}

static inline void readScene(scene * s){
	// for locking a scene
	// multithreading and all that
	while(s->beingWritten){doNothing();}
	s->readers++;
}

static inline void doneReadingScene(scene * s){
	// above, but reverse
	s->readers--;
}

static inline void releaseScene(scene * s){
	// dangerous
	s->readers = 0;
	s->beingWritten = 0;
}

//////////////////////
// removing shapes

// templates are c++ only? say that again lol
// thanks to Salem from cprogramming forums
// https://cboard.cprogramming.com/c-programming/178701-like-templates-cplusplus-but-not-really.html

void removeTriangle(scene* s, triangle* obj){
	size_t i = obj - (s->triangles);
	if (obj < s->triangles || i > s->triangleCount){
		printf("Memory corruption\n");
		abort();
	}
	// copy last elem to obj to be removed
	s->triangles[i] = s->triangles[s->triangleCount];
	// decrement, in effect removing the last element
	s->triangleCount--;
}

void removeSphere(scene* s, sphere* obj){
	size_t i = obj - (s->spheres);
	if (obj < s->spheres || i > s->sphereCount){
		printf("Memory corruption\n");
		abort();
	}
	s->spheres[i] = s->spheres[s->sphereCount];
	s->sphereCount--;
}

sphere* makeSphere(scene* s, vec3 position, float radius, vec3 velocity, material* mat){

	writeScene(s);

	if (mat==NULL){
		mat = &(s->mats[s->matCount++]);
		memset(mat,0,sizeof(*mat));
		mat->color = random3();
		mat->reflectance = 1;
	} else {
		s->mats[s->matCount] = *mat;
		//free(mat);
		mat = &(s->mats[s->matCount++]);
	}

	const float density = 1;

	sphere* const sph = &(s->spheres[s->sphereCount++]);
	memset(sph, 0, sizeof(*sph));
	sph->mat = mat;
	sph->position = position;
	sph->radius = radius;
	sph->velocity = velocity;
	sph->mass = density*M_PI*powf(radius, 2);

	doneWritingScene(s);

	return sph;
}

void clearAllSpheres(scene * s){

	writeScene(s);

	s->sphereCount = 0;

	doneWritingScene(s);
}

void sceneMakeBVH(scene* s){
	writeScene(s);
	freeBVH(s->bvh);
	s->bvh = buildBVH(s->triangles, s->triangleCount, 0, &s->bb);
	printAABB(&s->bb);
	doneWritingScene(s);
}

void freeScene(scene * s){
	printf("Deleting cam\n");
	free(s->cam);
	free(s->triangles);
	free(s->spheres);
	free(s->mats);
	free(s);
}

material *addMaterial(scene *s, material m){
	material *ptr = &s->mats[s->matCount++];
	*ptr = m;
	return ptr;
}

void addTriangle(scene *s, triangle t){
	triangle *ptr = &s->triangles[s->triangleCount++];
	*ptr = t;
}

void loadOBJ(scene *s, const char* filename, material *m, vec3 translation) {
	FILE* file = fopen(filename, "r");
	if (!file) {
		perror("Error: Cannot open file\n");
		return;
	}

	vec3 *vertices = calloc(256, sizeof *vertices);
	size_t vertexCount = 0;
	size_t vertexCap = 256;

	writeScene(s);

	size_t n = 0;

	char line[128];
	while (fgets(line, sizeof(line), file)) {
		if (line[0] == 'v' && line[1] == ' ') {
			// Parse vertex
			vec3 v;
			sscanf(line, "v %f %f %f", &v.x, &v.y, &v.z);
			vertices[vertexCount++] = add3(v, translation);
			if (vertexCount >= vertexCap){
				vertexCap *= 2;
				vertices = reallocarray(vertices, vertexCap, sizeof *vertices);
			}
		} 
		else if (line[0] == 'f' && line[1] == ' ') {
			// Parse face (triangle only)
			int i0, i1, i2, i3;
			if (sscanf(line, "f %d %d %d", &i0, &i1, &i2) == 3) {
				// OBJ indices start from 1, so subtract 1
				triangle tmp = (triangle){
					.mat = m,
					.v0 = vertices[i0 - 1],
					.v1 = vertices[i1 - 1],
					.v2 = vertices[i2 - 1]
				};
				tmp.normal = nTriangle(&tmp);
				addTriangle(s, tmp);
				n++;
			}
			if (sscanf(line, "f %d//%*d %d//%*d %d//%*d", &i0, &i1, &i2) == 3) {
				triangle tmp = (triangle){
					.mat = m,
					.v0 = vertices[i0 - 1],
					.v1 = vertices[i1 - 1],
					.v2 = vertices[i2 - 1]
				};
				tmp.normal = nTriangle(&tmp);
				addTriangle(s, tmp);
				n++;
			}
			else if (sscanf(line, "f %d %d %d %d", &i0, &i1, &i2, &i3) == 4) {
				triangle tmp = (triangle){
					.mat = m,
					.v0 = vertices[i0 - 1],
					.v1 = vertices[i1 - 1],
					.v2 = vertices[i2 - 1]
				};
				tmp.normal = nTriangle(&tmp);
				addTriangle(s, tmp);
				tmp = (triangle){
					.mat = m,
					.v0 = vertices[i0 - 1],
					.v1 = vertices[i2 - 1],
					.v2 = vertices[i3 - 1]
				};
				tmp.normal = nTriangle(&tmp);
				addTriangle(s, tmp);
				n+=2;
			} else {
				printf("UNKOWN LINE: '%s'\n", line);
			}
		}
	}
	doneWritingScene(s);
	free(vertices);
	sceneMakeBVH(s);
	fclose(file);
	printf("Loaded %zu vertices, %zu triangles\n", vertexCount, n);
}
