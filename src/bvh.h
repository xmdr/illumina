#pragma once
#include <stdlib.h>

#include "geometry.h"

typedef struct BVH BVH;

struct BVH {
	// Only in branch nodes
	AABB lbb;
	AABB rbb;
	BVH *lt; 
	BVH *rt;

	// Only in leaf nodes
	triangle *triangles;
	size_t n;
};

int compareX(const void* a, const void* b) {
	triangle* ta = (triangle*)a;
	triangle* tb = (triangle*)b;
	float xa = (ta->v0.x + ta->v1.x + ta->v2.x)/3.f;
	float xb = (tb->v0.x + tb->v1.x + tb->v2.x)/3.f;
	return 1-2*(xa > xb);
}

int compareY(const void* a, const void* b) {
	triangle* ta = (triangle*)a;
	triangle* tb = (triangle*)b;
	float ya = (ta->v0.y + ta->v1.y + ta->v2.y)/3.f;
	float yb = (tb->v0.y + tb->v1.y + tb->v2.y)/3.f;
	return 1-2*(ya > yb);
}

int compareZ(const void* a, const void* b) {
	triangle* ta = (triangle*)a;
	triangle* tb = (triangle*)b;
	float za = (ta->v0.z + ta->v1.z + ta->v2.z)/3.f;
	float zb = (tb->v0.z + tb->v1.z + tb->v2.z)/3.f;
	return 1-2*(za > zb);
}

BVH* buildBVH(triangle* triangles, size_t n, int d, AABB *const bb) {
	if (n == 0) {
		return NULL;
	}

	BVH* node = calloc(1, sizeof*node);

	// create a leaf node
	if (n <= TRIANGLES_PER_BV) {
		node->triangles = triangles;
		node->n = n;
		node->lt = NULL;
		node->rt = NULL;
		*bb = AABBFromTriangles(triangles, n);

		return node;
	}

	int axis = d % 3;  // X, Y, or Z
	__compar_fn_t cmp[3] = {compareX, compareY, compareZ};
	qsort(triangles, n, sizeof(triangle), cmp[axis]);
	
	size_t medianIndex = n / 2;
	triangle* l = triangles;
	triangle* r = triangles + medianIndex;

	node->lt = buildBVH(l,   medianIndex, d+1, &node->lbb);
	node->rt = buildBVH(r, n-medianIndex, d+1, &node->rbb);

	*bb = mergeAABB(node->lbb, node->rbb);

	return node;
}

void freeBVH(BVH *b){
	if (b == NULL){
		return;
	}

	freeBVH(b->lt);
	freeBVH(b->rt);

	free(b);
}
