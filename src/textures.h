#pragma once
#include "vector.h"

vec3 checkerboard(vec3 pos)
{
	pos = scale3(pos, 0.5);
	const int x = (int)floor(pos.x+.5);
	const int y = (int)floor(pos.y+.5);
	const int z = (int)floor(pos.z+.5);

	if ( (x+y+z)%2 ){
		return (vec3){1,1,1};
	} else {
		return (vec3){.1,.1,.1};
	}
}

vec3 sun(vec3 dir){
	float v = dot3(dir, SUN_DIR);
	if (v>SUN_COSTHETA){
		return (vec3){SUN_LUMINOSITY,SUN_LUMINOSITY,SUN_LUMINOSITY};
	}
	return (vec3){0,0,0};
}

vec3 sky(vec3 dir){
	float y = (dir.y);
	return lerp3((vec3){0.5,0.7,1}, (vec3){0.1,0.18,0.33}, fabsf(y*y));
}

vec3 defaultBkg(vec3 dir){
	return add3(sun(dir), sky(dir));
}

vec3 normalMap(vec3 dir){
	return (vec3){0.5+0.5*dir.x, 0.5+0.5*dir.y, 0.5+0.5*dir.z};
}

vec3 ambientBkg(vec3 dir){
	(void)dir;
	return (vec3){12.f/255,6.f/255,20.f/255};
}

vec3 spaceBkg(vec3 dir){
	return mult3(sun(dir), (vec3){1.f, 232.f/255, 55.f/255});
}

vec3 blackBkg(vec3 dir){
	(void)dir;
	return (vec3){0,0,0};
}
