// You must recompile for settings to take effect!

#define TARGET_FRAMETIME_MS 16 // integer 1000ms/(fps)
// Frame rate limiter
// 24fps:41 30fps:33 60fps:16 75fps:13 120fps:8 144fps:7

//#define ENABLE_PHYSICS
// Enable physics (only works on spheres)
// To configure physics, see src/physics.h

#define MULTI_PROCESSED_RENDERING
// Use all available cores

#define CHECKERBOARD_STEPS 2
// A full frame is rendered in 2 passes if CHECKERBOARD_STEPS is 2, and in 100 passes if it is set to 100.
// Excessive values may cause excessive screen tearing, until the scene stops moving.
// Undef to disable

#define BACKFACE_CULLING
// Speed up triangle intersections, at the cost of correct refraction

// #define FLAT_CASTING
// Render the direct material color of the nearest object

#define MAX_DEPTH 4
// Number of times a ray can bounce before it's terminated. Higher = better soft shadows, ambient occlusion, global illumination

#define SAMPLES 8
// How many paths per pixel. Higher = less noise and fewer fireflies

// #define SUN_BIAS 1
// Importance sampling: 1/100% chance that a ray is biased towards the sun.

#define SUN_LUMINOSITY 100
#define SUN_COSTHETA 0.995
// For realistic sun size: 1919 arc seconds => 0.9999567220625517, but also double the sun brightness if you do that. Smaller and brighter = more noise

#define SUN_DIR (vec3){0.577350269,0.577350269,0.577350269}
// norm(1,1,1)

#define MAX_RENDER_DISTANCE 1000000000
// speaks for itself

#define R_OFFS 0.0001
// raytracing: new origin offset along normal

#define TRIANGLES_PER_BV 2
// Amount of triangles per BVH leaf node
