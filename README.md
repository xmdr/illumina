# Illumina

...is a realtime, multi-threaded raytracing engine on top of SDL2. It should be easily portable, but it's written for and tested on Linux.

If you insist, you can also crank up the samples per pixel and recursion depth in `config.h` to make renders like this. Be warned, it turns into an offline renderer.

Screenshots rendered at 2560x1440 with 8192 samples per pixel and up to 8 bounces per ray:

![Screenshot](https://www.michaelroeleveld.nl/illumina/spheres_2560_1440_8192spp_8d.png)

![Screenshot](https://www.michaelroeleveld.nl/illumina/hel_2560x1440_8192spp_8d.png)



## Compile Illumina:

**Compilation requires GCC compiler and SDL2, OpenMP development libraries**

- Pick the desired scene in `main.c`, line 31
- Configuration: `config.h`
- `$ make`

## Run Illumina:

`$ ./bin/illumina.exe resolutionX resolutionY [scale/fullscreen]`

For now, scale must be an integer > 0.

16:9 resolutions and scalings to fit on 1920x1080:

- `384 216 5`
- `480 270 4`
- `640 360 3`
- `1280 720 2`

To run it in fullscreen, simply add the letter "f", instead of a scale.

`$ ./bin/illumina.exe 480 270 f`


## Controls:

0. `W` `A` `S` `D` or arrow keys work as expected: Forward/Left/Backward/Right respectively.
1. use the `E`/`Q` keys to move the camera up/down along it's relative vertical axis.
2. Hold `l-ctrl` to speed up, `l-shift` to slow down temporarily.
3. Use the `scroll` wheel to change the camera's panning speed.
4. `RMB drag` the mouse to change the camera angle.
5. `LMB` on something in the scene to change its color to something random.
6. `Shift` `LMB` on something in the scene to remove it.
7. Press `enter` to dump the current camera position and lookat vector.
8. Press `f` to follow an object.
9. Press `shift` `f` to unfollow.
10. Press `space` to create a sphere.
11. Press `backspace` to clear all spheres.

## Future plans:

- Add denoising pass
- Add bloom
- Add HDR tonemapping, exposure controls
- Put renderer on GPU
- Mesh abstraction, collision
- Importance sample all emitters
