#include "../src/scene.h"

void setupDemoScene(void){
	vec3 origin, lookat;

	origin = (vec3){5.904644, 3.107351, -11.194409};
	lookat = (vec3){0.691269, 0.107346, -6.417320};

	material *red_glossy = addMaterial(workspace, (material){
		.roughness = 1,
		.reflectance = 1,
		.gloss_roughness = 0.3,
		.gloss_reflectance = 0.1,
		.color = rgb2vec("970e13"),//ffeac8
	});

	material *cream_matte = addMaterial(workspace, (material){
		.roughness = 1,
		.reflectance = 1,
		.gloss_reflectance = 0,
		.color = rgb2vec("ffeac8"),
	});

	loadOBJ(workspace, "scenes/models/hel.obj", red_glossy, (vec3){0,0,0});
	loadOBJ(workspace, "scenes/models/nidhoggur.obj", cream_matte, (vec3){-10,3,0});
	positionCamera(workspace->cam, origin, lookat);
	workspace->bkgTexture = sky;
}