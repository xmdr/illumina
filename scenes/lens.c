#include "../src/scene.h"

void setupDemoScene(void){
	camera *cam = workspace->cam;
	
	initCamera(
		cam, 
		(vec3){0,0,10},
		(vec3){0,0,0},
		cam->aspect,
		cam->fov
	);

	makeSphere(
		workspace,
		(vec3){0,0,0},
		5,
		(vec3){0,0,0},
		addMaterial(workspace,(material){
			.IOR = 1.3,
			.reflectance = 0.04,
			.color = (vec3){0.96,0.96,0.96}
		})
	);

	float offset = -20;
	makeSphere(
		workspace,
		(vec3){8,8,offset+5},
		3,
		(vec3){0,0,0},
		addMaterial(workspace,(material){
			.reflectance = 1,
			.roughness = 1,
			.color = rgb2vec("fe2222")
		})
	);
	
	material *groundMaterial = addMaterial(workspace,(material){
		.texture = checkerboard,
		.reflectance = 1,
		.roughness = 1,
	});

	const vec3 n1 = (vec3){0,0,1};
	const float groundSize = 21; // radius
	workspace->triangleCount = 2;
	triangle * tri  = &(workspace->triangles[0]);
	tri->v2 = (vec3){-groundSize, groundSize,offset};
	tri->v1 = (vec3){ groundSize, groundSize,offset};
	tri->v0 = (vec3){ groundSize,-groundSize,offset};
	tri->normal = n1;
	tri->mat = groundMaterial;

	triangle * tri2 = &(workspace->triangles[1]);
	tri2->v2 = (vec3){ groundSize,-groundSize,offset};
	tri2->v1 = (vec3){-groundSize,-groundSize,offset};
	tri2->v0 = (vec3){-groundSize, groundSize,offset};
	tri2->normal = n1;
	tri2->mat = groundMaterial;

	sceneMakeBVH(workspace);
}
