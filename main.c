#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>

#include <SDL2/SDL.h>

#ifdef MULTI_PROCESSED_RENDERING
	#include <omp.h>
#endif

#include "config.h"

#include "src/random.h"
#include "src/extra_math.h"
#include "src/vector.h"
#include "src/textures.h"
#include "src/material.h"
#include "src/geometry.h"
#include "src/AABB.h"
#include "src/intersects.h"
#include "src/camera.h"
#include "src/scene.h"
scene *workspace;

#include "src/raytrace.h" // dependent on workspace
#include "scenes/obj.c" // same dependency

#include "src/bitmap.h"
#include "src/multithreading.h"
#include "src/userInput.h"
#ifdef ENABLE_PHYSICS
#include "src/physics.h"
#endif

int main(int argc, char *argv[])
{
	if (argc!=3 && argc!=4){
		printf("Wrong amount of args given: expected \n`./illumina width height [scale | f for fullscreen]`\n");
		return EXIT_FAILURE;
	}

	int fullscreen = 0;
	int scale = 1;
	if (argc==4){
		if (argv[3][0] == 'f'){
			fullscreen = 1;
		} else {
			scale = atoi(argv[3]);
			if (scale > 100 || scale < 1){
				printf("[ERROR] (recovered): Wrong scale, automatically set to 1.\n");
				scale = 1;
			}
		}
	}
	
	//srand(clock());
	srand(1337);

	int width = atoi(argv[1]);
	int height = atoi(argv[2]);
	int pitch = width*3;
	uint8_t *pixels;

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		fprintf(stderr, "SDL_Init Error: %s\n", SDL_GetError());
		return EXIT_FAILURE;
	}

	SDL_Window * window = SDL_CreateWindow(
		"Illumina (Rendering first frame)", 
		SDL_WINDOWPOS_UNDEFINED, 
		SDL_WINDOWPOS_UNDEFINED, 
		width*scale, 
		height*scale, 
		SDL_WINDOW_RESIZABLE
	);

	if (fullscreen){
		SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
	}

	SDL_Renderer * renderer;
	renderer = SDL_CreateRenderer(
		window,
		-1,
		SDL_RENDERER_ACCELERATED
	);

	SDL_Texture * buffer = SDL_CreateTexture(
		renderer,
		SDL_PIXELFORMAT_RGB24,
		SDL_TEXTUREACCESS_STREAMING, 
		width,height
	);

	printf("Setting up private variables...\n");

	char windowTitleFormat[] = "Illumina (%dfps / %dms)";
	char windowTitleBuffer[32] = {0};
	int fps = 0;
	int delta;

	triangle* triangles = calloc(99999, sizeof(*triangles)); // honestly it's not like you'll get any fps beyond this
	sphere*   spheres   = calloc(8192, sizeof(*spheres));
	material* mats      = calloc(8192, sizeof(*mats));
	camera*   cam       = calloc(1,    sizeof(*cam));
	initCamera(
		cam, 
		(vec3){0,0,0},
		(vec3){0,0,1},
		(float)width/height,
		70
	);

	// camera following
	cam->follow = (struct camFollow){
		.subject = NULL,
		.subjectType = '\0',
		.subjectVelocity = NULL,
		.subjectOldMat = NULL,
		.subjectMatPtr = NULL,
		.trackingMat = calloc(1, sizeof(material))
	};
	cam->follow.trackingMat->color = rgb2vec("00c832");

	cam->physics = (struct camPhysics){
		.velocity = (vec3){0,0,0},
		.mass = 1, //kg
		.springC = 50,
		.springDampening = 7.5,
		.springLength = 50,
	};

	printf("Setting up shared variables...\n");

	workspace = calloc(1, sizeof(scene)); // roblox reference, yes plox
	workspace->triangles = triangles;
	workspace->spheres = spheres;
	workspace->mats = mats;
	workspace->cam = cam;
	workspace->bkgTexture = &defaultBkg;
	setupDemoScene();

	int demoRunning = 1;

	printf("Setting up side-functionality threads...\n");
	struct thread_data * data = calloc(1, sizeof(struct thread_data));
	data->window = window;
	data->workspace = workspace;
	data->demoRunning = &demoRunning;
	data->texturePtr = &buffer;
	data->pitch = pitch;
	data->xres = width;
	data->yres = height;

	void * datapt = &data;

	SDL_Thread * ui_thread;
	ui_thread = SDL_CreateThread(ui_thread_main, "UserInputThread", datapt);
	if (ui_thread == NULL){
		printf("Error initing UI thread:\n");
		SDL_GetError();
		return EXIT_FAILURE;
	}

#ifdef ENABLE_PHYSICS
	SDL_Thread * psx_thread;
	psx_thread = SDL_CreateThread(psx_thread_main, "PhysicsThread", datapt);
	if (psx_thread == NULL){
		printf("Error initing physics thread:\n");
		SDL_GetError();
		return EXIT_FAILURE;
	}
#endif

	printf("\nStarting render...\nWidth: %d\nHeight: %d\n\n", width, height);
	printf("%zu triangles, %zu spheres\n", workspace->triangleCount, workspace->sphereCount);
	int startTime = 0;
	int frames = 0;
	uint32_t blockedRenders = 0;

	camera camcopy = *cam;

	startTime = SDL_GetTicks();

	while (++frames && demoRunning)
	{
		uint32_t blockedRendersThisFrame = 0;
		while(cam->locked){
			blockedRenders++;
			blockedRendersThisFrame++;
			if (blockedRendersThisFrame%0xffffff == 0){
				printf("[Renderer] Waiting for camera to unlock! (%d blocked renders)\n", blockedRendersThisFrame);
			}
			if (blockedRendersThisFrame > 0xfffffff0){
				printf("[Renderer] 0xffffff blocked renders this frame, something is wrong\n");
				abort();
			}
			// do nothing and wait until unlocked but every 100 missed renders
		}
		cam->locked = 1;
		camcopy = *cam;
		cam->locked = 0;

		SDL_LockTexture (buffer, NULL, (void **) &pixels, &pitch);

		vec3 origin = camcopy.origin;
		int index=0, u=0, v=0;
		vec3 fragColor;

		readScene(workspace); 
		// prevents another thread from editing workspace

#ifdef MULTI_PROCESSED_RENDERING
		#pragma omp parallel for schedule(dynamic, 1) shared(pixels,origin) private(index,u,v,fragColor) // OpenMP
#endif

		for (v = 0; v < height; ++v)
			{
#ifdef MULTI_PROCESSED_RENDERING
			#pragma omp parallel for schedule(dynamic, 1) shared(pixels,origin) private(index,u,fragColor)
#endif
#ifdef CHECKERBOARD_STEPS
			// alternate checkerboard
			// v+frames%2 will return the remainder of v divided by 2, eg either 0 or 1
			// essentially alternating the starting value for u each frame
			// When you add the two, then modulate(%) that, you get the complement checkerboard every other frame.
			for (u = (v + frames)%CHECKERBOARD_STEPS; u < width; u+=CHECKERBOARD_STEPS) // skip every other column
#else
			for (u = 0; u < width; ++u) // just render full frame as usual
#endif

			// for each collumn --> for each cell in row
			{
				const vec3 dir = normalize(sub3(cameraToWorld(&camcopy, ((float)u)/width, ((float)v)/height), origin));
				fragColor = raytrace(origin, dir, 0);
				/*fragColor = (vec3){
					sqrtf(fragColor.x) * 255,
					sqrtf(fragColor.y) * 255,
					sqrtf(fragColor.z) * 255,
				};*/
				fragColor = (vec3){
					fminf(255, (fragColor.x)*255),
					fminf(255, (fragColor.y)*255),
					fminf(255, (fragColor.z)*255),
				};

				index = (u*3) + (height-v-1)*pitch;
				// the reason that we render bottom to top is that the pixels pixelY=0 will get rendered at the top of the window
				// and we need to correct for that otherwise our image is upside down. It's just how SDL works. We also need to
				// offset by -1 pixel otherwise we get a black row of pixels at the top.

				pixels[index]   = fragColor.x;
				pixels[index+1] = fragColor.y;
				pixels[index+2] = fragColor.z;
			}
		}
		doneReadingScene(workspace);


		// OpenCL: synchronize devices here

		SDL_UnlockTexture(buffer);	
		SDL_RenderCopy(renderer, buffer, NULL, NULL);
		SDL_RenderPresent(renderer);

		delta = SDL_GetTicks() - startTime;
		startTime = SDL_GetTicks();

		const int leftToWait = (TARGET_FRAMETIME_MS - delta);

		if (leftToWait > 0){
			SDL_Delay(leftToWait);
			fps = (int)(1000/((float)(delta+leftToWait)));
		} else {
			fps = (int)(1000/((float)(delta)));
		}


		fps = (int)(1000.f/((float)(delta)));
		snprintf(windowTitleBuffer, sizeof(windowTitleBuffer)-1, windowTitleFormat, fps, delta);
		printf("%d\n", delta);
		SDL_SetWindowTitle(window, windowTitleBuffer);
	}

	printf("Succesful renders: %d\n", frames);
	printf("Blocked renders: %u\n", blockedRenders);
	printf("Cleaning up SDL stuff...\n");
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	//freeScene(workspace);

	printf("Main thread exited.\n");
	return EXIT_SUCCESS;
}

/*
// yes... this is exactly what you think it is... Ready your GPU for the next episode ;)
static inline void renderKernel(uint8_t * pixels, int i, int width, int height, int pitch, camera * camcopy, vec3 origin) 
{
	int x = i%width;
	int y = height - i/width -1; // always rounds down because of C int division, go bottom to top

	float u = (float)x/(width-1); // usually uv coordinates go from -1 to 1... these go from 0 to 1.
	float v = (float)y/(height-1);

	color3 fragColor = raytrace(
		origin, // origin of our ray

		normalize(sub3(cameraToWorld(camcopy, u, v), origin)),
		// ^ direction of our ray, get the vector3 direction that the camera is pointing at

		0, // start with recursion depth 0, because this is the zeroeth depth (surface)
		NULL, // don't ignore any objects' intersection
		1.f // index of refraction of the medium that ray is travelling through (air)
	);

	int index = (i/width)*pitch + x*3; 
	// ^ you might say "i*3;", except SDL2 4-byte-aligns each row so unless (pitch*3)%4 == 0, this will cause missteps.
	pixels[index]   = fragColor.r; // red
	pixels[index+1] = fragColor.g; // green
	pixels[index+2] = fragColor.b; // blue
}
*/
